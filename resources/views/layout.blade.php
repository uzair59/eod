@include('includes.header')
@include('includes.sidebar')
<div class='content'>
    @yield('content')
</div>
