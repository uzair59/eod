<div id="menu" class="hidden-print hidden-xs">
    <div class="sidebar sidebar-inverse">
        <div class="user-profile media innerAll">
            <a href="" class="pull-left"><img src="../assets/images/people/50/8.jpg" alt="" class="img-circle"><span class="badge badge-primary">2</span></a>
            <div class="media-body">
                <a href="" class="strong">Adrian Demian</a>
                <p class="text-success"><i class="fa fa-fw fa-circle"></i> Online</p>
            </div>
            <ul>
                <li class="active"><a href=""><i class="fa fa-fw fa-user"></i></a></li>
                <li><a href=""><i class="fa fa-fw fa-envelope"></i></a></li>
                <li><a href=""><i class="fa fa-fw fa-lock"></i></a></li>
            </ul>
        </div>
        <div class="sidebarMenuWrapper">
            <ul class="list-unstyled">
                <li><a href="profile_resume.html?lang=en"><span>Users</span></a></li>
                <li><a href="profile_resume.html?lang=en"><span>Tasks</span></a></li>
                <li><a href="profile_resume.html?lang=en"><span>Reports</span></a></li>
            </ul>
        </div>
    </div>
</div>