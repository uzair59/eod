<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html> <![endif]-->
<!--[if !IE]><!--><html><!-- <![endif]-->
    <head>
        <title>FLAT PLUS Template (v1.2.3)</title>

        <!-- Meta -->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

        <!--
        **********************************************************
        In development, use the LESS files and the less.js compiler
        instead of the minified CSS loaded by default.
        **********************************************************
        <link rel="stylesheet/less" href="/assets/less/admin/module.admin.page.index.less" />
        -->

        <!--[if lt IE 9]><link rel="stylesheet" href="/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
        <link rel="stylesheet" href="/assets/css/admin/module.admin.page.index.min.css" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="/assets/components/library/jquery/jquery.min.js?v=v1.2.3"></script>
        <script src="/assets/components/library/jquery/jquery-migrate.min.js?v=v1.2.3"></script>
        <script src="/assets/components/library/modernizr/modernizr.js?v=v1.2.3"></script>
        <script src="/assets/components/plugins/less-js/less.min.js?v=v1.2.3"></script>
        <script src="/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.2.3"></script>
        <script src="/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.2.3"></script>
        <script src="/assets/components/library/jquery-ui/js/jquery-ui.min.js?v=v1.2.3"></script>
        <script src="/assets/components/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js?v=v1.2.3"></script>
    </head>
    <body>
        <div class="navbar navbar-fixed-top navbar-primary main" role="navigation">
            <ul class="nav navbar-nav navbar-right hidden-xs">
                <li><a href="" class="menu-icon"><i class="fa fa-sign-out"></i></a></li>
            </ul>
        </div>